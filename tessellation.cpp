#include "tessellation.h"

///
/// \brief Tesselation::Tesselation - Constructor. Creates a tesselation like this:
/// ![http://i.imgur.com/WuMRMsi.png](http://i.imgur.com/WuMRMsi.png)
/// \param parent - parent of this tesselation, you should pass a reference to the window
/// where this tesselation 'lives'.
///
Tessellation::Tessellation(QWidget *parent) :
    QWidget(parent)
{
    resize(101,101);
    width = 50;
    height = 50;
    move(0,0);
    rotation = 0;
}

///
/// \brief Tesselation::getRotation - getter for the rotation.
///
int Tessellation::getRotation() { return rotation; }

///
/// \brief Tesselation::getWidth - getter for the tesseltation width
///
int Tessellation::getWidth()    { return width; }

///
/// \brief Tesselation::getHeight - getter for the tesseltation height
///
int Tessellation::getHeight()   { return height; }


///
/// \brief Tesselation::setRotation - setter for the tesselation rotation
/// \param r: rotation
///
void Tessellation::setRotation(int r) { rotation = r; }

///
/// \brief Tesselation::setWidth - setter for the tesselation width
/// \param w: width
///
void Tessellation::setWidth(int w)    { width = w; }


///
/// \brief Tesselation::setHeight - setter for the tesselation height
/// \param h: height
///
void Tessellation::setHeight(int h)   { height = h; }

//Tesselation & Tesselation::operator=(Tesselation &t) {
//    return *this;
//}

void Tessellation::paintEvent(QPaintEvent *event) {
    QPainter p(this);
    QPen myPen;

    myPen.setWidth(1);
    myPen.setColor(QColor(0x0000ff));
    p.setPen(myPen);

    // draw the cyan half
    p.setBrush(Qt::cyan);

    p.translate(width/2,height/2);
    p.rotate(rotation);
    p.translate(-width/2,-height/2);

    QPoint points[3];
    points[0].setX(width); points[0].setY(height);
    points[1].setX(0);   points[1].setY(height);
    points[2].setX(width); points[2].setY(0);
    p.drawPolygon(points, 3, Qt::OddEvenFill);

    // draw the blue half
    p.setBrush(Qt::blue);
    QPoint pp[3];
    pp[0].setX(0); pp[0].setY(0);
    pp[1].setX(0);   pp[1].setY(height);
    pp[2].setX(width); pp[2].setY(0);
    p.drawPolygon(pp, 3, Qt::OddEvenFill);

}
