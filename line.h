#ifndef LINE_H
#define LINE_H

#include <QWidget>
#include <QPainter>

class Line : public QWidget
{
    Q_OBJECT
public:
    explicit Line(QWidget *parent = 0);
    Line(int fromX, int fromY, int toX, int toY, int w, QColor c, QWidget *parent = 0);
    Line(int fromX, int fromY, int length, double angle, int w, QColor c, QWidget *parent = 0);
    void setCoords(int fromX, int fromY, int toX, int toY);
    void setpenColor(QColor c);
    void setPenWidth(int w);
    int getX0();
    int getY0();
    int getX1();
    int getY1();

signals:

public slots:

protected:
    void paintEvent(QPaintEvent *event);

private:
    int x0, y0, x1, y1;
    int penWidth;
    QColor penColor;

};

#endif // LINE_H
