# Tessellations

This lab is adapted from https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-2.

## Objectives

Throughout this exercise the students will practice:

* producing patterns using repetition structures
* objects
* functions

## Concepts

A tessellation (or tilling) is created when a shape is repeated to cover a surface leaving no gaps or overlaps. A **regular tessellation** is a pattern made by repeating a **regular polygon**, such as triangles, squares or hexagons. 

![http://www.mathnstuff.com/math/spoken/here/2class/150/regular.gif](http://www.mathnstuff.com/math/spoken/here/2class/150/regular.gif)

**Figure 1** - The only possible regular tesselations are obtained using triangles, squares or hexagons.


In this lab we will use square tesselations to practice producing patterns using nested loop.


## Library

The project available at ???? contains the class `Tesselation` (an abstraction of a square tesselation) and the class `drawingWindow`.  The following sample code creates a `drawingWindow` called *w* and a `Tesselation` called *t*. Observe that the function window's function `addTesselation` must be invoked in order for the tessellation to be actually painted. 

```cpp
int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    DrawingWindow w;        // creates the DrawingWindow object
    w.resize(300, 300);
    w.show();

    Tessellation t;          // creates the Tesselation object
    t.move(50,100);         // sets the position of the tessellation

    w.addTessellation(t);    // add the tessellation object to the window

    return a.exec();
}
```



<img src="http://i.imgur.com/JJwWlWl.png">

Here is an example of creating a function to draw four tesselletions in positions (0,0), (50,50), (100,100), and (150,150), with rotations 0, 90, 180 and 270 degrees.

```cpp
int foo(DrawingWindow& w) {
    int rot = 0;
    for (int i = 0; i < 4; i++) {
        Tessellation t;
        t.setRotation(rot);
        t.move(i * 50, i * 50);
        w.addTessellation(t);
        rot += 90;
    }

}

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    DrawingWindow w;
    w.resize(300, 300);
    w.show();

    foo(w);
    return a.exec();
}
```

Observe how the function `foo` needs to receive a reference to the window object since it is invoking its `addTesselation` function in each iteration.  

<img src="http://i.imgur.com/GKzWNCv.png">



The Qt project at *HERE????* contains the classes and the `foo` function. Download, build and run. You should observe the figure above.



## Exercise 1

Create a function `herringbone` to produce the following tessellation pattern:

<img src="http://i.imgur.com/u7nLnnC.png">

The size of the window is 400x400. The size of each tessellation square is its default 50x50.


## Exercise 2

Create a function `zigzag` to produce the following tessellation pattern:

<img src="http://i.imgur.com/wOS01xh.png">


## Exercise 3

Create a function `diamond` to produce the following tessellation pattern:

<img src="http://i.imgur.com/0vnI7rL.png">


### Deliverables

In the following text boxes, copy the functions that you developed for the program. Remember to properly comment all functions and use adequate indentation and variable naming practices.

### References 

<a name="Rocha"></a>[1] Rocha, Anderson, and Siome Goldenstein. "Steganography and steganalysis in digital multimedia: Hype or hallelujah?." Revista de Informática Teórica e Aplicada 15.1 (2008): 83-110.







