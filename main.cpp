
#include <QApplication>
#include <iostream>
#include <QLineEdit>
#include <QObject>
#include <QAction>
#include <QPushButton>
#include <cmath>
#include <QDebug>

#include "drawingWindow.h"
#include "tessellation.h"

// These are some of the tessellation patterns in:
// https://sites.google.com/a/wellesley.edu/wellesley-cs118-spring13/lectures-labs/lab-2


int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    DrawingWindow w;        // creates the DrawingWindow object
    w.resize(300, 300);
    w.show();

    Tessellation t;          // creates the Tesselation object
    t.move(50,100);         // sets the position of the tessellation

    w.addTessellation(t);    // add the tessellation object to the window

    return a.exec();
}


